# React Native Template

## Table of contents

- [Setting up a project](#setting-up-the-project)
- [Eslint](#eslint)
- [Prettier](#prettier)
- [Navigation](#navigation)
- [Atoms design](#atoms-design)
- [UI](#ui)
- [Expo](#expo)
- [React redux](#react-redux)
- [Navigation Part 2](#navigation-part-2)
- [Thunks](#thunks)

## Setting up a project 
### tag v1.0

> [React native](https://reactnative.dev/) combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces. (from https://reactnative.dev/)

> [Expo](https://docs.expo.dev/) is a framework and a platform for universal React applications. It is a set of tools and services built around React Native and native platforms that help you develop, build, deploy, and quickly iterate on iOS, Android, and web apps from the same JavaScript/TypeScript codebase. (from https://docs.expo.dev/)

> [Expo Go](https://expo.dev/client) Run your projects on your own device faster than ever, and share those projects across your whole team. (from https://expo.dev/client)

```bash
$ expo init mobile-client
$ cd mobile-client
$ npm start
```

## Eslint
### tag v2.0

> [ESLint](https://eslint.org/) is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code, with the goal of making code more consistent and avoiding bugs
> 
> ESLint statically analyzes your code to quickly find problems. ESLint is built into most text editors and you can run ESLint as part of your continuous integration pipeline.
> 
> Many problems ESLint finds can be automatically fixed. ESLint fixes are syntax-aware so you won't experience errors introduced by traditional find-and-replace algorithms. (from https://eslint.org/)

[ESLint plugin for React Native](https://www.npmjs.com/package/eslint-plugin-react-native)

```git
$ git reset --hard
git checkout v1.0
```

```bash
$ npm i --save-dev eslint
$ npm i --save-dev eslint-plugin-react
$ npm i --save-dev eslint-plugin-react-native
$ npm i –-save-dev eslint-plugin-json
$ eslint –-init
```
.eslintrc.js

```js
    module.exports = {
    env: {
        'react-native/react-native': true,
        es6: true,
    },
    extends: [
        'plugin:react/recommended',
        'airbnb',
        'plugin:json/recommended',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        ecmaFeatures: {
        jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [
        'react', 'react-native',
    ],
    rules: {
        'react-native/no-unused-styles': 2,
        'react-native/split-platform-components': 2,
        'react-native/no-inline-styles': 2,
        'react-native/no-color-literals': 2,
        'react-native/no-raw-text': 2,
        'no-use-before-define': 'off',
        'react/style-prop-object': 'off',
        'linebreak-style': 0,
    },
    ignorePatterns: ['node_modules/', 'assets/'],
    };
```

Update package.json scripts part:

```json
"scripts": {
    ...
    "lint": "eslint ./** --no-error-on-unmatched-pattern",
}
```

```bash
$ npm run lint
```

## Prettier
### tag v3.0

> Prettier is an opinionated code formatter with support for:
> 
> - JavaScript (including experimental features)
> - JSX
> - Angular
> - Vue
> - Flow
> - TypeScript
> - CSS, Less, and SCSS
> - HTML
> - JSON
> - GraphQL
> - Markdown, including GFM and MDX
> - YAML
> 
> It removes all original styling* and ensures that all outputted code conforms to a consistent style. (See this blog post)
> 
> Prettier takes your code and reprints it from scratch by taking the line length into account. (from https://prettier.io/docs/en/index.html)

```bash
$ npm install --save-dev prettier
$ echo {}> .prettierrc.json
{
  "trailingComma": "es5",
  "tabWidth": 2,
  "semi": true,
  "singleQuote": true
}


$ echo {}> .prettierignore
# Ignore artifacts: 
build  
coverage 
node_modules
assets
.*
package.*
```

Update package.json scripts part:

```json
"scripts": {
    ...
   "prettier": "npx prettier --write ./**"
}
```

## Navigation
### tag v4.0

React Navigation is made up of some core utilities and those are then used by navigators to create the navigation structure in your app! To frontload the installation work, let's also install and configure dependencies used by most navigators, then we can move forward with starting to write some code. (from https://reactnavigation.org/docs/getting-started/)

```bash
$ npm install @react-navigation/native
$ expo install react-native-screens react-native-safe-area-context
$ npm install @react-navigation/native-stack
```

Code details about navigation tag v4.0

## Atoms design
### tag v5.0

> Atom – the basic, easily reusable building block. For example label or button. 
> Molecule – a group of atoms bonded together. Designed to do one thing. For example a single form field.
> Organism – a group of molecules (or even atoms) to create a more complex part of your application that they can live on their own(for example, form).
> Template – a template is just a layout.
> Page – the instance of a template.

```bash
$ npm i prop-types
```

Code details about atoms design and props validation tag v5.0

## UI
### tag v6.0

> Paper is a collection of customizable and production-ready components for React Native, following Google’s Material Design guidelines. (from https://callstack.github.io/react-native-paper/)

```bash
$ npm install react-native-paper
```
Code details about UI using react-native-paper tag v6.0

## Expo
### tag v7.0

> Expo is a framework and a platform for universal React applications. It is a set of tools and services built around React Native and native platforms that help you develop, build, deploy, and quickly iterate on iOS, Android, and web apps from the same JavaScript/TypeScript codebase. (from https://docs.expo.dev/)

```bash
$ npm install @react-navigation/elements
$ expo install expo-camera
$ npm install react-native-tab-view
$ expo install react-native-pager-view
```
Code details about expo components tag v7.0

## React redux
### tag v8.0 and tag v9.0

Code details about react redux tag v8.0 v9.0

## Navigation Part 2
### tag v10.0

Code details about navigation part 2 tag v10.0

## Thunks
### tag v11.0

Code details about react thunks tag v11.0
