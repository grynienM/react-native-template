import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider as ReduxProvider } from 'react-redux';
import store from './src/app/redux/store';
import Navigation from './src/navigation/Navigation';
import useColorSchema from './src/app/hooks/useColorSchema';

export default function App() {
  const theme = useColorSchema();


  return (
    <PaperProvider theme={theme}>
      <ReduxProvider store={store}>
        <Navigation />
        <StatusBar style="auto" />
      </ReduxProvider>
    </PaperProvider>
  );
}
