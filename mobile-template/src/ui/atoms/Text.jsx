import React from 'react';
import PropTypes from 'prop-types';
import { Paragraph } from 'react-native-paper';

const Text = (props) => {
  const { children, style } = props;

  return <Paragraph style={style}>{children}</Paragraph>;
};

Text.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.node,
};

Text.defaultProps = {
  style: null,
};

export default Text;
