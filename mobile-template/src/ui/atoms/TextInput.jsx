import * as React from 'react';
import { TextInput as TextInputPaper } from 'react-native-paper';
import PropTypes from 'prop-types';

const TextInput = ({ text, setText, label }) => (
  <TextInputPaper label={label} value={text} onChangeText={(x) => setText(x)} />
);

export default TextInput;

TextInput.propTypes = {
  text: PropTypes.string,
  label: PropTypes.string,
  setText: PropTypes.func.isRequired,
};

TextInput.defaultProps = {
  text: '',
  label: '',
};
