import React from 'react';
import { Button as ButtonComponent } from 'react-native-paper';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { title, onPress, icon } = props;

  return (
    <ButtonComponent icon={icon} mode="contained" onPress={onPress}>
      {title}
    </ButtonComponent>
  );
};

Button.propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.string,
  icon: PropTypes.string,
};

Button.defaultProps = {
  title: null,
  onPress: () => {},
  icon: 'cursor-default-click',
};

export default Button;
