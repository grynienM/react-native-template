import React from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Text from '../atoms/Text';
import Colors from '../../consts/Colors';

const Paragraph = (props) => {
  const { children } = props;

  return <Text style={styles.paragraph}>{children}</Text>;
};

const styles = StyleSheet.create({
  paragraph: {
    color: Colors.silver,
    fontSize: 14,
  },
});

Paragraph.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Paragraph;
