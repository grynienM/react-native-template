import React from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Text from '../atoms/Text';
import Colors from '../../consts/Colors';

const Logo = (props) => {
  const { children } = props;

  return <Text style={styles.paragraph}>{children}</Text>;
};

const styles = StyleSheet.create({
  paragraph: {
    color: Colors.white,
    fontSize: 20,
  },
});

Logo.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Logo;
