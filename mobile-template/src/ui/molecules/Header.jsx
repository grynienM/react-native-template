import React from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Text from '../atoms/Text';
import Colors from '../../consts/Colors';

const Header = (props) => {
  const { children } = props;

  return <Text style={styles.paragraph}>{children}</Text>;
};

const styles = StyleSheet.create({
  paragraph: {
    color: Colors.black,
    fontSize: 24,
  },
});

Header.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Header;
