import * as React from 'react';
import { Appbar as AppbarPaper, Avatar } from 'react-native-paper';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';
import Logo from '../molecules/Logo';
import Text from '../atoms/Text';
import Colors from '../../consts/Colors';

const Appbar = ({ username, onLogoutPress, onLoginPress }) => {
  const navigation = useNavigation();

  const action =
    username !== '' ? (
      <AppbarPaper.Action
        icon="logout"
        color={Colors.white}
        onPress={onLogoutPress}
      />
    ) : (
      <AppbarPaper.Action
        icon="login"
        color={Colors.white}
        onPress={onLoginPress}
      />
    );
  return (
    <AppbarPaper>
      <View style={styles.container}>
        <View style={styles.logo}>
          <TouchableOpacity
            style={styles.avatar}
            onPress={() => navigation.openDrawer()}
          >
            <Avatar.Icon icon="menu" color={Colors.white} size={40} />
          </TouchableOpacity>
          <Logo>App Name</Logo>
        </View>
        <View style={styles.usernameWrapper}>
          <Text style={styles.username}>{username}</Text>
          {action}
        </View>
      </View>
    </AppbarPaper>
  );
};

const styles = StyleSheet.create({
  logo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    padding: 10,
  },
  container: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  username: {
    color: Colors.white,
  },
  usernameWrapper: {
    alignItems: 'center',
    marginLeft: 10,
    flexDirection: 'row',
  },
});

export default Appbar;

Appbar.propTypes = {
  username: PropTypes.string,
  onLogoutPress: PropTypes.func,
  onLoginPress: PropTypes.func,
};

Appbar.defaultProps = {
  username: '',
  onLogoutPress: () => {},
  onLoginPress: () => {},
};
