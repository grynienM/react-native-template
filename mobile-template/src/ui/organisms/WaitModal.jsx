import * as React from 'react';
import {
  Modal,
  Portal,
  Text,
  Provider,
  ActivityIndicator,
  Colors,
} from 'react-native-paper';
import PropTypes from 'prop-types';

const WaitModal = ({visible}) => {
  const containerStyle = {
    backgroundColor: 'white',
    padding: 20,
    margin: 20,
    flexDirection: 'column',
    alignItems: 'center',
  };

  return (
    <Provider>
      <Portal>
        <Modal
          visible={visible}
          contentContainerStyle={containerStyle}
        >
          <Text>Please wait...</Text>
          <ActivityIndicator animating color={Colors.red800} />
        </Modal>
      </Portal>
    </Provider>
  );
};

export default WaitModal;

WaitModal.propTypes = {
    visible: PropTypes.bool.isRequired
}