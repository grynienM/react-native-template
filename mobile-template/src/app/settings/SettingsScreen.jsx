import React from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import Page from '../templates/ApplicationPage';
import Paragraph from '../../ui/molecules/Paragraph';

const SettingsScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();

  const { id } = route.params;
  const header = `Settings screen id: ${id}`;
  const gotoOnPres = () => navigation.navigate('Camera', { id: 2 });

  return (
    <Page header={header} gotoLabel="Go to Camera" gotoOnPress={gotoOnPres}>
      <Paragraph>More details on the page</Paragraph>
    </Page>
  );
};

export default SettingsScreen;
