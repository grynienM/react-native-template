import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { View } from 'react-native';
import { authenticate } from '../redux/thunks/authenticate';
import Button from '../../ui/atoms/Button';
import Page from '../templates/Page';
import Header from '../../ui/molecules/Header';
import TextInput from '../../ui/atoms/TextInput';

const LoginScreen = () => {
  const [text, setText] = useState('');

  const dispatch = useDispatch();
  const login = () => dispatch(authenticate({ username: text }));

  return (
    <Page>
      <Header>Login screen</Header>
      <View>
        <TextInput label="Username: " text={text} setText={setText} />
        <Button title="Log in" icon="login" onPress={login} />
      </View>
    </Page>
  );
};

export default LoginScreen;
