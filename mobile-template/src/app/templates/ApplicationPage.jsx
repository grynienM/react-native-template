import React from 'react';
import { View, StyleSheet, useWindowDimensions } from 'react-native';
import { useHeaderHeight } from '@react-navigation/elements';
import PropTypes from 'prop-types';
import { Avatar, Card } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../ui/atoms/Button';
import Appbar from '../../ui/organisms/Appbar';
import { getCurrentUser } from '../redux/selectors';
import { LogoutUser, LoginUser } from '../redux/actions';

const LeftContent = (props) => <Avatar.Icon {...props} icon="folder" />;

const ApplicationPage = (props) => {
  const { children, header, gotoOnPress, gotoLabel } = props;
  const windowHeight = useWindowDimensions().height;
  const headerHeight = useHeaderHeight();
  const username = useSelector(getCurrentUser);
  const dispatch = useDispatch();

  const onLogoutUser = () => dispatch(LogoutUser());
  const onLoginUser = () => dispatch(LoginUser({ username: 'marcin' }));

  return (
    <Card style={{ height: windowHeight - headerHeight }}>
      <Appbar
        username={username}
        onLogoutPress={onLogoutUser}
        onLoginPress={onLoginUser}
      />
      <Card.Title title={header} subtitle="Card Subtitle" left={LeftContent} />
      <Card.Content style={styles.content}>
        <View>{children}</View>
      </Card.Content>
      <Card.Actions style={styles.actions}>
        <Button title={gotoLabel} onPress={gotoOnPress} />
      </Card.Actions>
    </Card>
  );
};
const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  actions: {
    justifyContent: 'flex-end',
  },
});

ApplicationPage.propTypes = {
  children: PropTypes.node.isRequired,
  header: PropTypes.string,
  gotoOnPress: PropTypes.func,
  gotoLabel: PropTypes.string,
};

ApplicationPage.defaultProps = {
  header: '',
  gotoOnPress: () => {},
  gotoLabel: '',
};

export default ApplicationPage;
