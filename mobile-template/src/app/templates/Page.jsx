import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Colors from '../../consts/Colors';

const Page = (props) => {
  const { children } = props;

  return <View style={styles.container}>{children}</View>;
};

export default Page;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: Colors.white,
  },
});

Page.propTypes = {
  children: PropTypes.node.isRequired,
};
