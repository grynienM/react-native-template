import React from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import Page from '../templates/ApplicationPage';
import Paragraph from '../../ui/molecules/Paragraph';

const HomeScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();

  const { id } = route.params;
  const header = `Home screen id: ${id}`;
  const gotoOnPres = () => navigation.navigate('Settings', { id: 1 });

  return (
    <Page header={header} gotoLabel="Go to Settings" gotoOnPress={gotoOnPres}>
      <Paragraph>More details on the page</Paragraph>
    </Page>
  );
};

export default HomeScreen;
