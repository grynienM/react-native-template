import React from 'react';
import { View, StyleSheet } from 'react-native';
import Paragraph from '../../ui/molecules/Paragraph';
import Colors from '../../consts/Colors';

const SecondTab = () => (
  <View style={styles.content}>
    <Paragraph>More details on the page</Paragraph>
  </View>
);

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: Colors.white,
    borderColor: Colors.yellow,
    borderWidth: 1,
    padding: 20,
  },
});

export default SecondTab;
