import React from 'react';
import { useWindowDimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import { useNavigation, useRoute } from '@react-navigation/native';
import Page from '../templates/ApplicationPage';
import FirstTab from './FirstTab';
import SecondTab from './SecondTab';

const renderScene = SceneMap({
  first: FirstTab,
  second: SecondTab,
});

const CameraScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();

  const { id } = route.params;
  const header = `Camera screen id: ${id}`;

  const gotoOnPres = () => navigation.navigate('Home', { id: 3 });
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'First' },
    { key: 'second', title: 'Second' },
  ]);

  return (
    <Page header={header} gotoLabel="Go to Home" gotoOnPress={gotoOnPres}>
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
      />
    </Page>
  );
};

export default CameraScreen;
