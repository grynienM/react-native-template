import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Camera } from 'expo-camera';
import Paragraph from '../../ui/molecules/Paragraph';
import Header from '../../ui/molecules/Header';
import Colors from '../../consts/Colors';
import Button from '../../ui/atoms/Button';

const FirstTab = () => {
  const [hasPermission, setHasPermission] = useState(null);
  const [cameraVisible, setCameraVisible] = useState(false);

  useEffect(() => {
    let isSubscribe = true;

    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();

      if (isSubscribe) {
        setHasPermission(status);
      }
    })();

    return () => {
      isSubscribe = false;
    };
  }, []);

  const toogleCameraLabel = cameraVisible ? 'Hide camera' : 'Show camera';

  return (
    <View style={styles.content}>
      <Header>Camera screen</Header>
      <Paragraph>Has permissions to camera: {hasPermission}</Paragraph>
      <View style={styles.container}>
        <Button
          title={toogleCameraLabel}
          icon="camera"
          onPress={() => setCameraVisible(!cameraVisible)}
        />
        {cameraVisible && <Camera style={styles.camera} />}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: Colors.white,
    borderColor: Colors.yellow,
    borderWidth: 1,
    padding: 20,
  },
  container: {
    marginTop: 10,
    height: 300,
  },
  camera: {
    width: 200,
    height: 200,
    marginTop: 10,
  },
});

export default FirstTab;
