import produce from 'immer';
import { APP_START_LOADING, APP_STOP_LOADING } from '../actionTypes';

const initialState = {
  isLoading: false,
};

export default function application(state = initialState, action) {
  const { type } = action;

  switch (type) {
    case APP_START_LOADING: {
      const appStartLoadingState = produce(state, (draft) => {
        draft.isLoading = true;
      });

      return appStartLoadingState;
    }
    case APP_STOP_LOADING: {
      const appStopLoadingState = produce(state, (draft) => {
        draft.isLoading = false;
      });

      return appStopLoadingState;
    }
    default:
      return state;
  }
}
