import produce from 'immer';
import { APP_LOGOUT_USER, APP_LOGIN_USER } from '../actionTypes';

const initialState = {
  currentUser: '',
  applicationInfo: {
    version: 1,
    name: 'App name',
  },
};

export default function applicationUser(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case APP_LOGOUT_USER: {
      const appLgoutUserState = produce(state, (draft) => {
        draft.currentUser = '';
      });

      return appLgoutUserState;
    }
    case APP_LOGIN_USER: {
      const { username } = payload;

      const appLoginUser = produce(state, (draft) => {
        draft.currentUser = username;
      });

      return appLoginUser;
    }
    default:
      return state;
  }
}
