import { combineReducers } from 'redux';
import applicationUser from './applicationUser';
import application from './application';

export default combineReducers({ applicationUser, application });
