export const getCurrentUser = (state) => state.applicationUser.currentUser;

export const isApplicationLoading = (state) => state.application.isLoading;
