import {
  APP_LOGIN_USER,
  APP_LOGOUT_USER,
  APP_START_LOADING,
  APP_STOP_LOADING,
} from './actionTypes';

export const LogoutUser = () => ({
  type: APP_LOGOUT_USER,
});

export const LoginUser = ({ username }) => ({
  type: APP_LOGIN_USER,
  payload: { username },
});

export const StartLoading = () => ({
  type: APP_START_LOADING,
});

export const StopLoading = () => ({
  type: APP_STOP_LOADING,
});
