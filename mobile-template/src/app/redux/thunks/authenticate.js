import { LoginUser, LogoutUser, StartLoading, StopLoading } from '../actions';
import { get } from '../../../api/apiClient';

export const authenticate =
  ({ username }) =>
  async (dispatch) => {
    try {
      dispatch(StartLoading());
      const result = await get('ok');

      if (result.data === true) {
        dispatch(LoginUser({ username }));
      } else {
        dispatch(LogoutUser());
      }
    } catch (err) {
      console.log(err);
    } finally {
      dispatch(StopLoading());
    }
  };
