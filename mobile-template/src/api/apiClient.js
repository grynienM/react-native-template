const axios = require('axios');

const API = 'http://localhost:3000/';

export const get = (url) =>
  new Promise((resolve, reject) => {
    axios
      .get(buildUrl(url))
      .then((response) => {
        // handle success
        resolve(response);
      })
      .catch((error) => {
        // handle error
        reject(error);
      });
  });

const buildUrl = (url) => `${API}${url}`;
