import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../app/home/HomeScreen';
import SettingsScreen from '../app/settings/SettingsScreen';
import CameraScreen from '../app/camera/CameraScreen';

const Drawer = createDrawerNavigator();

const UserNavigator = () => (
  <Drawer.Navigator
    screenOptions={{
      headerShown: false,
    }}
  >
    <Drawer.Screen
      name="Home"
      component={HomeScreen}
      initialParams={{ id: 0 }}
    />
    <Drawer.Screen
      name="Settings"
      component={SettingsScreen}
      initialParams={{ id: 0 }}
    />
    <Drawer.Screen
      name="Camera"
      component={CameraScreen}
      initialParams={{ id: 0 }}
    />
  </Drawer.Navigator>
);

export default UserNavigator;
