import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useSelector } from 'react-redux';
import LoginScreen from '../app/login/LoginScreen';
import { getCurrentUser, isApplicationLoading } from '../app/redux/selectors';
import UserNavigator from './UserNavigator';
import WaitModal from '../ui/organisms/WaitModal';

const Stack = createNativeStackNavigator();

export default function Navigation() {
  const currentUser = useSelector(getCurrentUser);
  const visible = useSelector(isApplicationLoading);

  return (
    <>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          {currentUser === '' ? (
            <Stack.Screen component={LoginScreen} name="Login" />
          ) : (
            <Stack.Screen
              name="UserNavigator"
              component={UserNavigator}
              options={{ headerShown: false }}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
      <WaitModal visible={visible} />
    </>
  );
}
