const Colors = {
  silver: '#71706e',
  blue: 'blue',
  black: 'black',
  yellow: '#f1c40f',
  white: 'white',
};

export default Colors;
