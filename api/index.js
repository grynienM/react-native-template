const express = require('express')
var cors = require('cors');
const app = express()
app.use(cors());
const port = 3000

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header("Access-Control-Allow-Headers", "x-access-token, Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/ok', (req, res) => {
	setTimeout(() => {
		res.send(true)
	}, 5000)  
})

app.get('/not', (req, res) => {
  res.send(false)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})